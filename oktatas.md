# SEO oktatás
- Search engine optimization
- Nagyjából 200 féle szempontot vesz figyelembe a Google. Ezek egy része publikus; egy részük sejthető és nem hivatalos forrásokban megtalálható; egy részük pedig teljesen titkos. Ebből a 200 szempontból nagyjából 35 "elég" szokott lenni, ezek az előadás témája.

## Title tag
- aloldalanként legyen egyedi
- használjuk a weboldalunkat leíró kulcsszavakat: milyen keresőkifejezések beírására szeretnénk hogy a weboldalunk megjelenjen a keresés elején (pl: villanyszerelés)
- ez fog leghangsúlyosabban megjelenni a Google találati listájában

## Description tag
- < meta name="description" content="<3 mondatnyi leírás>">
- 80-200 karakter
- aloldalanként legyen egyedi
- használjuk a kulcsszavakat
- ez is meg fog jelenni a Google találati listában, az oldal címe alatt

## Ragok
- merjünk ragozni, a kulcsszavaink ragozott alakjait használni, a Google ismeri ezeket, sőt, el is várja hogy használjuk őket

## Szinonímák
- a Google szinoníma készlete is fejlett, úgyhogy használjuk a kulcsszavaink szinonímáit is

## Url cím
- domain név + mappanevek + paraméterek
- egy honlap csak egy domainen legyen, különben a Google bűnteti az oldal besorolásnál a duplikált tartalmat
- minél öregebb egy domain, annál jobb, megbízhatóbb  a Google szemében
- itt is jó ha megjelenik egy fő kulcsszó
- fiatal, most alakult vállalkozások ne a nevüket használják domain névként

## Kulcsszavak
- ne legyen keyword stuffing, egy kulcsszót elég egyszer leírni
- < meta name="keywords" content="<2-3 kulcsszó vesszővel elválasztva>">
- aloldalanként eltérő lehet
- ma már nem annyira vizsgálja ezt mint régen
- legyenek ott a kulcsszavak az oldal szövegében is
- a kulcsszavak sűrűsége legyen 2-6%: ahogy élőszóban is beszélnénk a témáról, nagyjából 3 mondatonként

## Szövegkörnyezet
- szavak, amik összeillenek a kulcsszavainkkal (villanyszerelés -> gyors kiszállás; szakember; megbízható)

## Helyesírás
- a Google bűntetni a helyesírási hibákat

## Címsor formátumok következetes használata
- h1, h2, h3, ...
- a kulcsszavak szerepeljenek a címben

## Linkek, hivatkozások
- linkek szövege legyen konzisztens a tartalommal
- breadcrumb
- tört linkek megszűntetése
- visszacsatolások gyűjtése (backlinking: az adott oldalra hány link vezet, akár saját weboldalunkról is)
- kimenő linkek kerülése
- bejövő linkek gyűjtése: (blogok és linkgyűjtő oldalak felkeresése, pl startlap)
- Egy jó módszer bejövő linkek gyűjtésére: Fórumokra regisztrálás több felhasználóval, az egyik nevében felteszünk egy kérdést, a másikkal pedig ajánljuk a weboldalunkat
- Ne egyik napról a másikra árasszuk el a fórumokat a weboldalunk linkjével, hanem apránként, fokozatosan.
- "linkcsere" nem jó, ezt a Google észreveszi

## Képek
- alt tag kitöltése, innen tudja a Google (és a képernyőolvasók) hogy mi van a képen (persze ezzel át is lehet verni)

## Reszponzivitás
- vannak mobile-friendly test oldalak, amik tanácsokat is adnak, hogy hogy lehetne még mobilbarátabb a weboldalunk

## Oldalbetöltés sebessége
- vannak speed test oldalak, amik szintén tudnak tanácsokat adni, hogy lehetne gyorsabb a betöltés
- ne használjunk túl nagy képeket
- kliens oldali kód tömörítése

## Kódminőség
- ne legyenek JS hibák
- szerkezeti elemek használata (tárolótípusok: footer, header, article, section)

## Konkurens honlapok vizsgálata
- mennyire felelnek meg a SEO irányelveknek, mennyire lesz nehéz "lekörözni" őket

## Aloldalak mennyisége
- legyen minél több (az egyre divatosabb onepage oldalak hátrányban vannak)

## Látogatók átlagos időtartalma
- Próbáljuk növelni az oldal figyelemmegtartó erejét
- Erre jó módszer a videó
- Google analytic-on belül ezt is lehet elemezni

## Látogatók száma
- A népszerű oldalakat előrébb sorolja a Google, de a sejtés szerint a fiatal oldalaknál ezt nem vizsgálja a Google

## Közösségi média megjelenések
- Jelenjen meg a weboldalunk linkje a különböző közösségi média posztjaiban

## Tudjunk és merjünk hibázni az apróbb dolgokban
- ezeket a Google elnézi, sőt, díjazza
- nem szereti a Google ha minden szempont tökéletesen meg van valósítva, mert akkor az az érzése, hogy csak át akarjuk verni az algoritmust
- pl a képek 20%ánál ne legyen kitöltve az alt tag 

## Google search console
- webhelytérkép (xml) készítése, feltöltése a webhelyre, tárhelyre. Ekkortól kezdve elemzi a Google a weboldalt és a látogatók interakcióit

## Az optimalizálás eredménye napokban, hetekben, hónapokban mérhető
